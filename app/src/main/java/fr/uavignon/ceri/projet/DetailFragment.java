package fr.uavignon.ceri.projet;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;

public class DetailFragment extends Fragment {
    public static final String TAG = DetailFragment.class.getSimpleName();

    private DetailViewModel viewModel;
    private TextView textArteName, textArteBrand, textArteDesc, textArteUsable, textArteYear;
    private ImageView imgArte;


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(DetailViewModel.class);

        // Get selected city
        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        String artefactId = args.getArtefactNum();
        Log.d(TAG,"selected id=" + artefactId);
        viewModel.setArtefact( artefactId );

        listenerSetup();
        observerSetup();

    }


    private void listenerSetup() {

        textArteName = getView().findViewById(R.id.dispName);
        textArteBrand = getView().findViewById(R.id.dispBrand);
        textArteDesc = getView().findViewById(R.id.dispDesc);
        textArteUsable = getView().findViewById(R.id.dispUsable);
        textArteYear = getView().findViewById(R.id.dispYear);

        imgArte = getView().findViewById(R.id.thumbnail);

        getView().findViewById(R.id.buttonUpdate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Récupération des données",
                            Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
            }
        });

        getView().findViewById(R.id.buttonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(fr.uavignon.ceri.projet.DetailFragment.this)
                        .navigate(R.id.action_DetailFragment_to_ListFragment);
            }
        });
    }

    private void observerSetup() {

        viewModel.getArtefact().observe(getViewLifecycleOwner(),
                artefact -> {
                    if (artefact != null) {
                        Log.d(TAG, "observing city view");

                        textArteName.setText(artefact.getName());
                        textArteBrand.setText(artefact.getBrand());
                        textArteDesc.setText(artefact.getDescription());

                        String a = "Non-Utilisable";
                        if( artefact.getUsable() ) a = "Utilisable";

                        textArteUsable.setText(a);
                        textArteYear.setText(String.valueOf(artefact.getYear()));

                        if( artefact.getThumbnail() != null )
                        {
                            Glide.with( this ).load( artefact.getThumbnail() ).into( imgArte );
                        }

                    }
                });
    }


}