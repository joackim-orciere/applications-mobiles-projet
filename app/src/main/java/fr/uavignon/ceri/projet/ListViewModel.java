package fr.uavignon.ceri.projet;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;

import fr.uavignon.ceri.projet.data.Artefact;
import fr.uavignon.ceri.projet.data.MuseumRepository;

public class ListViewModel extends AndroidViewModel {
    private MuseumRepository repository;
    private MutableLiveData<ArrayList<Artefact>> allArtefacts;

    public ListViewModel (Application application) {
        super(application);
        repository = MuseumRepository.get(application);

        allArtefacts = new MutableLiveData<>();
        allArtefacts = repository.getArtefactList();
    }

    public void deleteArtifact(String id) {
        repository.deleteArtefact( id );
    }

    LiveData<ArrayList<Artefact>> getAllArtefacts() {
        return allArtefacts;
    }

    public void deleteCity(String id) {
        repository.deleteArtefact( id );
    }

    public void loadCollection()
    {
        Thread t = new Thread()
        {
            public void run()
            {
                repository.loadCollection();
            }
        };
        t.start();
    }

}

