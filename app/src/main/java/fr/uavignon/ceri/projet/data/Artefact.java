package fr.uavignon.ceri.projet.data;


import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;


@Entity(tableName = "Artefact", indices = {@Index(value = {"name", "description"},
        unique = true)})
public class Artefact {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name="_id")
    private String id;

    @NonNull
    @ColumnInfo(name="name")
    private String name;

    @ColumnInfo(name="description")
    private String description = null;

    @ColumnInfo(name="brand")
    private String brand = null;

    @NonNull
    @ColumnInfo(name="year")
    private int year = -1;

    @ColumnInfo(name="thumbnail")
    private String thumbnail = null;

    @NonNull
    @ColumnInfo(name="usable")
    private boolean usable = false;

    public Artefact(String id, String name, String description, int year)
    {
        this.id = id;
        this.name = name;
        this.description = description;
        this.year = year;
    }

    @Ignore
    public Artefact() {

    }

    @NonNull
    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    @NonNull
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        if( this.description == null ) return "no information available";
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getBrand()
    {
        if( brand != null ) return brand;
        else return "unknown manufacturer";
    }

    public void setBrand(String brand)
    {
        this.brand = brand;
    }

    public int getYear()
    {
        return year;
    }

    public void setYear(int year)
    {
        this.year = year;
    }

    public void setUsable(boolean isUsable) { this.usable = isUsable; }

    public boolean getUsable()
    {
        return this.usable;
    }

    public void setThumbnail(String thumbnail)
    {
        this.thumbnail = thumbnail;
    }

    public String getThumbnail()
    {
        return thumbnail;
    }

}
