package fr.uavignon.ceri.projet.data;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import java.net.HttpCookie;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import fr.uavignon.ceri.projet.data.database.ArtefactDao;
import fr.uavignon.ceri.projet.data.database.ArtefactRoomDatabase;
import fr.uavignon.ceri.projet.data.webservice.ArtefactResponse;
import fr.uavignon.ceri.projet.data.webservice.ArtefactResult;
import fr.uavignon.ceri.projet.data.webservice.MuseumInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

import static fr.uavignon.ceri.projet.data.database.ArtefactRoomDatabase.databaseWriteExecutor;

public class MuseumRepository {

    private static final String TAG = MuseumRepository.class.getSimpleName();


    private MutableLiveData<Artefact> selectedArtefact;

    private ArtefactDao artefactDao;

    private final MuseumInterface api;


    private static volatile MuseumRepository INSTANCE;
    private MutableLiveData<ArrayList<Artefact>> artefactList = new MutableLiveData<>();

    public synchronized static MuseumRepository get(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new MuseumRepository(application);
        }

        return INSTANCE;
    }

    public MuseumRepository(Application application) {
        ArtefactRoomDatabase db = ArtefactRoomDatabase.getDatabase(application);
        artefactDao = db.artefactDao();
        selectedArtefact = new MutableLiveData<>();

        Retrofit retrofit=
                new Retrofit.Builder()
                        .baseUrl("https://demo-lia.univ-avignon.fr/cerimuseum/")
                        .addConverterFactory(MoshiConverterFactory.create())
                        .build();

        api = retrofit.create(MuseumInterface.class);
    }

    public MutableLiveData<Artefact> getSelectedArtefact() {
        return selectedArtefact;
    }




    public long insertArtefact(Artefact newArtefact) {
        Future<Long> flong = databaseWriteExecutor.submit(() -> {
            return artefactDao.insert(newArtefact);
        });
        long res = -1;
        try {
            res = flong.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (res != -1)
            selectedArtefact.setValue(newArtefact);
        return res;
    }

    public int updateArtefact(Artefact artefact) {
        Future<Integer> fint = databaseWriteExecutor.submit(() -> {
            return artefactDao.update(artefact);
        });
        int res = -1;
        try {
            res = fint.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (res != -1)
            selectedArtefact.setValue(artefact);
        return res;
    }

    public void deleteArtefact(String id) {
        databaseWriteExecutor.execute(() -> {
            artefactDao.deleteArtifact(id);
        });
    }

    public void getArtefact(String id)  {
        Future<Artefact> arte = databaseWriteExecutor.submit(() -> {
            Log.d(TAG,"selected id="+id);
            return artefactDao.getArtefactById(id);
        });
        try {
            selectedArtefact.setValue(arte.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void loadCollection() {

        api.getCollection().enqueue(
                new Callback<Map<String, ArtefactResponse>>() {
                    @Override
                    public void onResponse(Call<Map<String, ArtefactResponse>> call,
                                           Response<Map<String, ArtefactResponse>> response) {

                        Log.d("onResponse", response.body().toString());
                        ArrayList<Artefact> artifactList = new ArrayList<>();
                        int i = 0;

                        for(String id: response.body().keySet())
                        {
                            ArtefactResult.transferInfo(response.body().get(id), artifactList, id);

                            insertArtefact(artifactList.get(i));
                            i++;
                        }

                        MuseumRepository.this.artefactList.setValue(artifactList);
                    }

                    @Override
                    public void onFailure(Call<Map<String, ArtefactResponse>> call, Throwable t) {
                        Log.d("onFailure", t.getMessage());
                    }
                });
    }

    public MutableLiveData<ArrayList<Artefact>> getArtefactList()
    {
        return artefactList;
    }


}
