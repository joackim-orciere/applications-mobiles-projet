package fr.uavignon.ceri.projet.data.database;

import androidx.lifecycle.MutableLiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.ArrayList;
import java.util.List;

import fr.uavignon.ceri.projet.data.Artefact;

@Dao
public interface ArtefactDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(Artefact artefact);

    @Query("DELETE FROM Artefact")
    void deleteAll();

    @Query("Delete FROM Artefact where _id = :id")
    void deleteArtifact(String id);

    @Query("SELECT * from Artefact ORDER BY name ASC")
    List<Artefact> getAllArtefacts();

    @Query("SELECT * from Artefact ORDER BY name ASC")
    List<Artefact> getSynchrAllCities();

    @Query("DELETE FROM Artefact WHERE _id = :id")
    void deleteArtefact(String id);

    @Query("SELECT * FROM Artefact WHERE _id = :id")
    Artefact getArtefactById(String id);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    int update(Artefact artefact);
}
