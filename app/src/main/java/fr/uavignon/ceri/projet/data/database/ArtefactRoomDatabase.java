package fr.uavignon.ceri.projet.data.database;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import fr.uavignon.ceri.projet.data.Artefact;

@Database(entities = {Artefact.class}, version = 4, exportSchema = false)
public abstract class ArtefactRoomDatabase extends RoomDatabase {

    private static final String TAG = ArtefactRoomDatabase.class.getSimpleName();

    public abstract ArtefactDao artefactDao();

    private static ArtefactRoomDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 1;
    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);


    public static ArtefactRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (ArtefactRoomDatabase.class) {
                if (INSTANCE == null) {
                    // Create database here
                            // without populate

                    INSTANCE =
                            Room.databaseBuilder(context.getApplicationContext(),
                                    ArtefactRoomDatabase.class,"artefact_database").fallbackToDestructiveMigration()
                                    .build();



                            // with populate
                    /*
                            INSTANCE =
                            Room.databaseBuilder(context.getApplicationContext(),
                                    ArtefactRoomDatabase.class,"artefact_database").fallbackToDestructiveMigration().build();
                    */
                }
            }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback =
            new RoomDatabase.Callback(){

                @Override
                public void onOpen (@NonNull SupportSQLiteDatabase db){
                    super.onOpen(db);

                    databaseWriteExecutor.execute(() -> {
                        // Populate the database in the background.
                        ArtefactDao dao = INSTANCE.artefactDao();

                    });

                }
            };

    /*
    private static RoomDatabase.Callback sRoomDatabaseCallback =
        new RoomDatabase.Callback(){

            @Override
            public void onOpen (@NonNull SupportSQLiteDatabase db){
                super.onOpen(db);

                databaseWriteExecutor.execute(() -> {
                    // Populate the database in the background.
                    CityDao dao = INSTANCE.cityDao();
                    dao.deleteAll();

                    Artefact[] cities = {new Artefact("Avignon", "France"),
                    new Artefact("Paris", "France"),
                    new Artefact("Rennes", "France"),
                    new Artefact("Montreal", "Canada"),
                    new Artefact("Rio de Janeiro", "Brazil"),
                    new Artefact("Papeete", "French Polynesia"),
                    new Artefact("Sydney", "Australia"),
                    new Artefact("Seoul", "South Korea"),
                    new Artefact("Bamako", "Mali"),
                    new Artefact("Istanbul", "Turkey")};

                    for(Artefact newCity : cities)
                        dao.insert(newCity);
                    Log.d(TAG,"database populated");
                });

            }
        };
        */


}
