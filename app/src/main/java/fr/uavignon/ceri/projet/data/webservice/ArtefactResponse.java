package fr.uavignon.ceri.projet.data.webservice;

import java.util.Map;

public class ArtefactResponse {

    public final String name = null;

    public final String description = null;

    public final String brand = null;

    public final Integer year = -1;

    public final Boolean usable = false;

    public final String thumbnails = null;
}
