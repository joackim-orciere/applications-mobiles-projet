package fr.uavignon.ceri.projet.data.webservice;

import java.util.ArrayList;

import fr.uavignon.ceri.projet.data.Artefact;
import fr.uavignon.ceri.projet.data.webservice.ArtefactResponse;

public class ArtefactResult {

    public static void transferInfo(ArtefactResponse artifactInfo, ArrayList<Artefact> artifacts, String id)
    {
        Artefact artifact = new Artefact();

        artifact.setName( artifactInfo.name );
        artifact.setDescription(artifactInfo.description);
        artifact.setUsable(artifactInfo.usable);

        artifact.setId(id);
        artifact.setBrand(artifactInfo.brand);
        artifact.setThumbnail("https://demo-lia.univ-avignon.fr/cerimuseum/items/" + id + "/thumbnail");
        artifact.setYear(artifactInfo.year);

        // return filled object
        artifacts.add(artifact);

    }



}
