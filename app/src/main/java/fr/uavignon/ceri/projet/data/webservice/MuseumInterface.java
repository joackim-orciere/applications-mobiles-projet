package fr.uavignon.ceri.projet.data.webservice;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface MuseumInterface {
    @Headers( {
            "Accept: json",
    })
    @GET("collection")
    public Call<Map<String, ArtefactResponse>> getCollection();
}
